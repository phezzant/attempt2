#ifndef DEVM_H
#define DEVM_H

#include <stdio.h>
#include <unistd.h>

#define ABS(X) ((X) < 0 ? (-X) : (X))  // ensure unsigned

#define BUFFERSIZE 255
#define MAX_DEVICES 10

#define NEW_DEVICE_PIPE "./new_devices"
#define KNOWN_DEV_FILE "./known_devices"
#define DEVM_LOG "./devm.log"
#define DEVM_PID "./devm.pid"

typedef struct {
    int type;
    int usb;
    int vendorid;
    int deviceid;
    pid_t pid;
} Device;

#endif



