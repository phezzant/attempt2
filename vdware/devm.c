#include "devm.h"

/* global variables */
FILE * log_fp;
FILE * pid_fp;
FILE * new_dev_pipe;
Device devices[MAX_DEVICES];
int num_devs = 0;
int connected_devs = 0;

const char CHAR_SET[] =
    "0123456789"
    "-=[]\\;',./"
    "abcdefghijklmnopqrstuvwxyz" // lowercase set
    ")!@#$%^&*("
    "_+{}|:\"<>?"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; // uppercase set

/* 
 *
 * FILE HANDLERS
 *
 */

void open_log() {
    log_fp = fopen(DEVM_LOG, "a");
}

void update_log(char * msg) {
    char buf[BUFFERSIZE];
    sprintf(buf, "[%ld] %s\n", (long) time(NULL), msg);
    fwrite(buf, sizeof(char), strlen(buf), log_fp);
    fflush(log_fp);
}

void close_log() {
    fclose(log_fp);
}

void write_pid() {
    pid_fp = fopen(DEVM_PID, "w");
    fprintf(pid_fp, "%d\n", (int) getpid());
    fclose(pid_fp);
}

void sigterm_pid() {
    unlink(DEVM_PID);
}

void open_new_dev_pipe() {
    new_dev_pipe = fopen(NEW_DEVICE_PIPE, "r");
}

void close_dev_pipe() {
    fclose(new_dev_pipe);
}

/* 
 *
 * UTIL FUNCTIONS
 *
 */

void init_devs() {

    FILE * fp;
    char buff[BUFFERSIZE];
    int type = DEV_NOT_CONNECTED;

    if ((fp = fopen(KNOWN_DEV_FILE, "r")) == NULL) {
        perror("Could not open known_devices file");
        exit(1);
    }

    while (fgets(buff, BUFFERSIZE, fp) != NULL) {
        for (int i = 0; buff[i] != 0; i++) {
            if (isupper(buff[i])) {
                buff[i] = tolower(buff[i]);
            }
        }
        if (strstr(buff, "mouse") != NULL) {
            type = MOUSE_TYPE;
        }
        if (strstr(buff, "keyboard") != NULL) {
            type = KEYBOARD_TYPE;
        }

        /* initialise device structs */
        if (type) {
            devices[num_devs].type = type;
            devices[num_devs].pid = -1;
            sscanf(buff, "id %x:%x", &devices[num_devs].vendorid, &devices[num_devs].deviceid);
            num_devs ++;
        }
        type = DEV_NOT_CONNECTED;
    }
    fclose(fp);
}

int find_device(int v_id, int d_id) {
    for (int i = 0; i < num_devs; i++) {
        if (devices[i].vendorid == v_id && devices[i].deviceid == d_id) {
            return i;
        }
    }
    return -1;
}

void sigterm_devm() {

    update_log("SIGTERM caught, devm closing");

    for (int i = 0; i < num_devs; i++) {
        if (devices[i].pid != DEV_NOT_CONNECTED) {
            kill(devices[i].pid, SIGTERM);
        }
    }
    // ensure all pipes are closed AND devm.pid is removed
    close_log();
    close_dev_pipe();
    sigterm_pid();
    exit(0);
}

void mouse_driver(FILE * fp0, FILE * fp1, Device * d) {

    int ch[3];
    unsigned char bytes[3];
    char buff[BUFFERSIZE];

    /* read unsigned char and convert to int ch */
    while ((ch[0] = fgetc(fp0)) != EOF) {
        ch[1] = fgetc(fp0);
        ch[2] = fgetc(fp0);

        bytes[0] = ch[0];
        bytes[1] = ch[1];
        bytes[2] = ch[2];

        /* if received an EOF partway through the packet --> Log error */
        if (ch[1] == EOF || ch[2] == EOF) {
            sprintf(buff, "Error while receiving packet: mouse %04x:%04x at dev/USB%d",
                    d->vendorid, d->deviceid, d->usb);
            update_log(buff);
            return;
        }

        sprintf(buff, "Device packet: 0x%02x%02x%02x from mouse %04x:%04x at dev/USB%d", 
                bytes[0], bytes[1], bytes[2], d->vendorid, d->deviceid, d->usb);
        update_log(buff);

        { 
            int dx, dy;
            char sc, left, middle, right, xsign, ysign;

            sc = bytes[2] & 0x3;
            left = (bytes[0] & L_BUTTON) ? 1 : 0;
            right = (bytes[0] & R_BUTTON) ? 1 : 0;
            middle = (bytes[0] & M_BUTTON) ? 1 : 0;
            xsign = (bytes[0] & X_SIGN) ? 1 : 0;
            ysign = (bytes[0] & Y_SIGN) ? 1 : 0;
            dx = (((bytes[1] << 8) + bytes[2]) >> 2) & 0x7;
            dy = (((bytes[1] << 8) + bytes[2]) >> 9) & 0x7;

            sprintf(buff, "dx: %c%d, dy: %c%d, sc: %c%d, left: %s, middle: %s, right:%s\n",
                    (xsign ? '-' : '+'),
                    dx,
                    (ysign ? '-' : '+'),
                    dy,
                    ((sc == 2 || sc == 0) ? '+' : '-'),
                    (sc > 0 ? '1' : '0'),
                    (left ? "on" : "off"), (middle ? "on" : "off"),(right ? "on" : "off"));
        }
        fwrite(buff, sizeof(char), strlen(buff), fp1);
    }
}

void keyboard_driver(FILE * fp0, FILE * fp1, Device * d) {

    unsigned char byte;
    int ch;
    char buff[BUFFERSIZE];
    char caps, shift;

    while ((ch = fgetc(fp0)) != EOF) {
        byte = ch;
        sprintf(buff, "Device packet: 0x%02x from keyboard %04x:%04x at dev/USB%d",
                byte, d->vendorid, d->deviceid, d->usb); 
        update_log(buff);

        /* use BITWISE & operator to determine if the key is either shift or caps */
        caps = byte & CAPS;
        shift = byte & SHIFT;

        /* if shift or caps, convert charset */
        if (shift) {
            byte = byte & ~(CAPS | SHIFT);
            sprintf(buff, "%c\n", CHAR_SET[byte+45]);
        } else if (caps) {
            byte = byte & ~(CAPS | SHIFT);
            if (byte >= 20) {
                sprintf(buff, "%c\n", CHAR_SET[byte+45]);
            } else {
                sprintf(buff, "%c\n", CHAR_SET[byte]);
            }
        } else {
            sprintf(buff, "%c\n", CHAR_SET[byte]);
        }
        /* write to output */
        fwrite(buff, sizeof(char), strlen(buff), fp1);
    }
}

void devmgr_signal (int signo) {
    if (signo == SIGTERM) {
        fflush(NULL);
    }
}

void device_manager (int dev_num) {

    Device *d = &devices[dev_num];
    char buff[BUFFERSIZE];
    FILE *fp_dev, *fp_usb;

    signal(SIGTERM, devmgr_signal);

    /* initiate the USB%d output file */
    sprintf(buff, USB_PIPE, d->usb);
    fp_dev = fopen(buff, "r");

    sprintf(buff, OUTPUT_FILE, d->usb);
    fp_usb = fopen(buff, "a");

    /* run device specific driver */
    switch (d->type) {
        case MOUSE_TYPE:
            mouse_driver(fp_dev, fp_usb, d);
            break;
        case KEYBOARD_TYPE:
            keyboard_driver(fp_dev, fp_usb, d);
            break;
        default:
            perror("Device connection problem");
            exit(4);
    }

    /* upon termination of the device, update the devm.log */
    sprintf(buff, "Device disconnected: %s %04x:%04x at dev/USB%d",
            (d->type == MOUSE_TYPE ? "mouse" : "keyboard"),
            d->vendorid, 
            d->deviceid,
            d->usb);
    update_log(buff);
    fclose(fp_dev);
    fclose(fp_usb);
}

void device_connected() {

    char buff[BUFFERSIZE];
    int v_id, d_id, dev_num;
    // fork a new device process
    pid_t pid;
    
    if((fgets(buff, BUFFERSIZE, pid_fp)) == NULL) {
        perror("Problem reading the device pid");
        exit(2);
    }

    sscanf(buff, "%x:%x", &v_id, &d_id);
    // find the device struct corresponding to the pid read in
    // return error if < 0
    if ((dev_num = find_device(v_id, d_id)) < 0) {
        perror("device not in known_devices");
        exit(3);
    } else {
        devices[dev_num].usb = connected_devs;
        connected_devs++;
        sprintf(buff, "Device connected: %s %04x:%04x at dev/USB%d",
                (devices[dev_num].type == MOUSE_TYPE ? "mouse" : "keyboard"),
                devices[dev_num].vendorid,
                devices[dev_num].deviceid,
                devices[dev_num].usb);
        update_log(buff);

        /* Create a new process for a the child device,
         * (if we are in the parent process, initialise log the process id of the parent process)
         * */
        if (!(pid = fork())) {
            /* child process */
            device_manager(dev_num);
        } else {
            devices[dev_num].pid = pid;
        }
    }
}

void sig_handler(int signo) {
    switch(signo) {
        case SIGTERM:
            sigterm_devm();
            break;
        case SIGUSR1:
            // new device connected, update_log()
            device_connected();
            break;
        default:
            break;
    }
}
            
int main() {

    write_pid();
    open_log();
    update_log("devm started");

    init_devs();

    /* catch signals */
    signal(SIGTERM, sig_handler);
    signal(SIGUSR1, sig_handler);

    /* hint - create an output directory! */
    mkdir(OUTPUT_DIR, 0777);

    open_new_dev_pipe();

    for(;;sleep(1));



    return 1;


}
