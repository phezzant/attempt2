#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <ctype.h>

#include <sys/stat.h>

#define BUFFERSIZE 255

#define DEVM_LOG "./devm.log"
#define DEVM_PID "./devm.pid"
#define KNOWN_DEV_FILE "./known_devices"
#define NEW_DEVICE_PIPE "./new_devices"
#define OUTPUT_DIR "./output"
#define USB_PIPE "./dev/USB%d"
#define OUTPUT_FILE "./output/USB%d"

#define ONE_FLAG 0x08
#define Y_SIGN 0x20
#define X_SIGN 0x10
#define M_BUTTON 0x04
#define R_BUTTON 0x02
#define L_BUTTON 0x01

#define CAPS 0x80
#define SHIFT 0x40

#define MAX_DEVICES 10

#define DEV_NOT_CONNECTED 0
#define MOUSE_TYPE 1
#define KEYBOARD_TYPE 2

typedef struct {
    int type;
    int usb;
    int vendorid;
    int deviceid;

    pid_t pid;
} Device;
